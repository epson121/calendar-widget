
var Calendar = {

	init : function() {
		var self = this;
		this.firstInMonth = moment().startOf('month');

		this.calendarData = document.getElementById("calendar-data");
		this.calendarWeeks = this.calendarData.getElementsByTagName('ul');
		console.log(this.calendarWeeks.item(2));
		this.monthNameElement = document.getElementById('current-month');

		this.prev = document.getElementById('prev');
		this.next = document.getElementById('next');

		this.prev.addEventListener('click', function() {
			self.firstInMonth.subtract(1, 'month');
			self.render();
		});

		this.next.addEventListener('click', function() {
			self.firstInMonth.add(1, 'month');
			self.render();
		});

		this.render();
	},

	render : function() {
		// day inweek for the start of month
		//this.calendarData.innerHTML = "";

		var day = this.firstInMonth.day();
		var daysInMonth = this.firstInMonth.daysInMonth();
		this.monthNameElement.innerHTML = this.firstInMonth.format('MMMM YYYY');

		var dayCounter = 1;

		// 5 * 7 box
		for (var i = 0; i < 6; i++) {

			// if counter >= num of days in current month,
			// display none this last ul;

			var ul = this.calendarWeeks.item(i);

			console.log(i);
			console.log(dayCounter > daysInMonth);
			if (i == 5 && dayCounter > daysInMonth) {
				ul.style = "display:none";
			}

			var lis = ul.getElementsByTagName('li');

			for (j = 0; j < 7; j++) {
				var li = lis.item(j);
				li.innerHTML = "";

				if (j == 0 || j == 6) li.className = "weekend";

				var label = (j + 1);
				if (i > 0 || j >= day) {
					if (dayCounter > daysInMonth) {
						dayCounter++;
						continue;
					}
					var div = document.createElement('div');
					div.id = "input_container";
					div.className = "day-div";

					var input = document.createElement('input');
					input.type = "text";
					input.className = 'day-input';
					input.maxLength="8";

					var img = document.createElement('img');
					img.src = "images/day-input/" + dayCounter + ".png";
					img.className = "day-image";

					div.appendChild(input);
					div.appendChild(img);

					li.appendChild(div);
					dayCounter++;
				}
			};
		};
	}

}

Calendar.init();